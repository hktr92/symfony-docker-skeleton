FROM php:8.0.1-cli-alpine3.12

RUN apk add --update --no-cache \
        git \
        curl \
        coreutils \
        wget \
        bash

COPY --from=composer:2.0.9 /usr/bin/composer /usr/bin
COPY --from=mlocati/php-extension-installer:1.2.0 /usr/bin/install-php-extensions /usr/bin/
RUN wget https://get.symfony.com/cli/installer -O - | bash
RUN mv $HOME/.symfony/bin/symfony /usr/bin/symfony

# PHP performance optimization: opcache
RUN install-php-extensions opcache
# Symfony extensions
RUN install-php-extensions intl zip
# Dev / debug extensions
RUN install-php-extensions xdebug-3.0.2

# install fixuid
RUN curl -SsL https://github.com/boxboat/fixuid/releases/download/v0.5/fixuid-0.5-linux-amd64.tar.gz | tar -C /usr/local/bin -xzf - && \
    chown root:root /usr/local/bin/fixuid && \
    chmod 4755 /usr/local/bin/fixuid && \
    mkdir -p /etc/fixuid && \
    printf "user: www-data\ngroup: www-data\n" > /etc/fixuid/config.yml

WORKDIR /usr/src/symfony

ENV PHP_FPM_WORKERS=${PHP_FPM_WORKERS:-5}

USER www-data
ENTRYPOINT ["fixuid", "-q"]
