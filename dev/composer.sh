#!/bin/bash

if [[ -z "$(command -v realpath)" ]]; then
  realpath() {
    [[ $1 = /* ]] && echo "$1" || echo "${PWD}/${1#./}"
  }
fi

script_dir="$(realpath "$0")"
bin_dir="$(dirname "${script_dir}")"
root_dir="$(dirname "${bin_dir}")"

docker run -t --rm \
    --mount type=bind,source="${root_dir}",target=//usr/src/symfony \
    --user="$(id -u):$(id -g)" \
    --entrypoint=fixuid \
    $(docker build -f "${root_dir}/Dockerfile" "${root_dir}" -q) -q symfony composer "$@"
